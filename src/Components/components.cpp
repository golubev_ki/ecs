//
// Created by Kirill on 26.01.2020.
//
#include "components.h"
#include "../Simulation/Simulation.h"
#include "../Observers/observers.h"

void ecs::Component::notify_observers(bool just_created) {
    if (obs_array == nullptr) {
        throw std::runtime_error("no observers provided");
    }
    for (auto &obs : *obs_array) {
        obs->observe(this, just_created);
    }
}

ecs::Component::Component(ecs::entity_id owner, observer_array *obs_ptr, ecs::comp_type type)
        : owner(owner), obs_array(obs_ptr), type(type) {}

