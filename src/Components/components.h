//
// Created by Kirill on 26.01.2020.
//
#include <memory>
#include "../utils.h"
#include <string>

#ifndef ECS_COMPONENTS_H
#define ECS_COMPONENTS_H
namespace ecs {
    class observer_array;

    struct Component {
    protected:
        observer_array *obs_array = nullptr;/*no ownership*/
        entity_id owner = -1;
        std::unique_ptr<size_t> hash;
        comp_type type{};

        [[nodiscard]] virtual size_t calculate_hash() const = 0;

    public:
        void notify_observers(bool just_created= false);

        [[nodiscard]] entity_id get_owner() const{
            return owner;
        }

        [[nodiscard]] comp_type get_type() const {
            return type;
        }

        [[nodiscard]] bool has_changed() const {
            return *hash != calculate_hash();
        }

        void reset_hash() {
            *hash = calculate_hash();
        }

        Component(entity_id owner, observer_array* obs_ptr, comp_type type);

        Component(Component &&) = default;

        Component &operator=(Component &&) = default;

        Component() = default;

        virtual ~Component() = default;

        Component(Component &) = delete;

        Component &operator=(Component &) = delete;
    };

#define ADD_DEFAULT_METHODS(name, struct_name) \
public:\
    struct_name(entity_id owner, observer_array* obs_ptr): Component(owner, obs_ptr, name){\
        notify_observers(true);\
    }\
    \
    struct_name() = default;\
    struct_name(struct_name&&) = default;\
    struct_name(const struct_name&) = delete;\
    struct_name& operator=(struct_name&&) = default;\
    struct_name& operator=(const struct_name&) = delete;\
    size_t calculate_hash() const override {\
        auto char_repr = reinterpret_cast<const char*>(this);\
        auto n = sizeof(struct_name);\
        return std::hash<std::string>()(std::string(char_repr, n));\
    }


#define ADD_COMPONENT(name, struct_name, impl) impl

#include "components.def"

#undef ADD_COMPONENT

}


#endif //ECS_COMPONENTS_H
