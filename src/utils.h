//
// Created by Kirill on 26.01.2020.
//


#ifndef ECS_UTILS_H
#define ECS_UTILS_H

#define PROTO_CONCAT(a, b) a##b
#define CONCAT(a, b) PROTO_CONCAT(a,b)

namespace ecs{
    /*generating enum type with each element corresponding to component type*/
    enum comp_type {
#define ADD_COMPONENT(name, struct_name, struct_impl) name,

#include "Components/components.def"

#undef ADD_COMPONENT
    };
    enum  obs_type{
#define ADD_OBSERVER(name, struct_name, impl) name,

#include "Observers/observers.def"
#undef ADD_OBSERVER
    };

    /*generating forward declarations for component types*/
    struct Component;

#define ADD_COMPONENT(name, struct_name, struct_impl) struct struct_name;

#include "Components/components.def"

#undef ADD_COMPONENT

    typedef int entity_id;
}

#endif //ECS_UTILS_H
