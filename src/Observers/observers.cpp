//
// Created by Kirill on 26.01.2020.
//
#include "observers.h"
#include "../Components/components.h"

#define ADD_OBSERVER(name, struct_name, impl)\
    void ecs:: struct_name ::observe(Component* comp, bool just_created){\
        if((comp->has_changed() or just_created) and comp->get_type() == observed_type and\
            not recorded_changes.contains(comp->get_owner())){\
            recorded_changes.insert(comp->get_owner());\
        }\
    }\
\
    inline void ecs:: struct_name ::observe(Component* comp){\
        observe(comp, false);\
    }\
    \
    inline void ecs:: struct_name :: clear(){\
        recorded_changes.clear();\
    }



#include "observers.def"

#undef ADD_OBSERVER