//
// Created by Kirill on 26.01.2020.
//
#include "../utils.h"
#include <boost/container/flat_set.hpp>

#ifndef ECS_OBSERVERS_H
#define ECS_OBSERVERS_H

namespace ecs {
    class Observer{
    public:
        virtual void observe(Component*, bool just_created) = 0;
        virtual inline void observe(Component*) = 0;
        virtual void clear() = 0;
        virtual ~Observer() = default;
    };
    template<comp_type type>
    class Proto_observer : Observer{
    public:
        const comp_type observed_type = type;
        ~Proto_observer() override = default;
    };

#define ADD_OBSERVER(name, struct_name, impl) impl

#include "observers.def"

#undef ADD_OBSERVER
}

#endif //ECS_OBSERVERS_H
