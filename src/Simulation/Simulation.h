//
// Created by Kirill on 26.01.2020.
//

#include <array>
#include "../../external_dependencies/magic_enum/include/magic_enum.hpp"
#include "../utils.h"
#include "../Observers/observers.h"

#ifndef ECS_SIMULATION_H
#define ECS_SIMULATION_H

namespace ecs {
    class observer_array : public std::array<std::unique_ptr<Observer>, magic_enum::enum_count<obs_type>()>{
    public:
        using std::array<std::unique_ptr<Observer>, magic_enum::enum_count<obs_type>()>::array;
    };

    class Simulation {
    private:
        observer_array future = {
#define ADD_OBSERVER(name, struct_name, impl)\
                std::unique_ptr<Observer>(dynamic_cast<Observer*>(new struct_name())),
#undef ADD_OBSERVER
        };
        observer_array present = {
#define ADD_OBSERVER(name, struct_name, impl)\
                std::unique_ptr<Observer>(dynamic_cast<Observer*>(new struct_name())),
#undef ADD_OBSERVER
        };
    };
}


#endif //ECS_SIMULATION_H
