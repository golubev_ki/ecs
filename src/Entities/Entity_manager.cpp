//
// Created by Kirill on 26.01.2020.
//

#include "Entity_manager.h"
#include "Entity.h"
#include <string>

#define ADD_COMPONENT(name, struct_name, imp) \
        inline const ecs:: struct_name& ecs::Entity_manager:: CONCAT(get_, CONCAT(name, _componet_instance)) (ecs::entity_id id) const{\
            return CONCAT(name, s).at(id);\
        }\
        inline ecs:: struct_name& ecs::Entity_manager:: CONCAT(get_, CONCAT(name, _componet_instance)) (ecs::entity_id id){\
            return CONCAT(name, s).at(id);\
        }\
        \
        ecs::Entity& ecs::Entity_manager:: CONCAT(add_, CONCAT(name, _component))(ecs::Entity& ent){\
            if(CONCAT(name, s).find(ent.id) == CONCAT(name, s).end()){\
                throw(std::runtime_error("one entity(id: " + std::to_string(ent.id) + ") can't have two components of the same type"));\
            }\
            if(ent.sig[int(name)]){\
                throw(std::runtime_error("one entity(id: " + std::to_string(ent.id) + ") can't have two components of the same type"));\
            }\
            if(obs_ptr == nullptr){\
                throw(std::runtime_error("can't spawn entity if no simulation attached"));\
            }\
            CONCAT(name, s)[ent.id] = std::move(struct_name(ent.id, obs_ptr));\
            return ent;\
        }

#include "../Components/components.def"

#undef ADD_COMPONENT
