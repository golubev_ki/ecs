//
// Created by Kirill on 26.01.2020.
//

#ifndef ECS_ENTITY_MANAGER_H
#define ECS_ENTITY_MANAGER_H

#include <vector>
#include <unordered_map>
#include "../Components/components.h"
#include "Entity.h"

namespace ecs {
    class Entity_manager {
        observer_array *obs_ptr = nullptr;/*no ownership*/
        entity_id cur_id = 0;
        std::unordered_map<entity_id, Entity> entities;
        template <comp_type type>
        Entity& add_component(Entity& ent){
            throw std::runtime_error("you are trying to add non-existing component type");
        }
#define ADD_COMPONENT(name, struct_name, imp) \
    private:\
        std::unordered_map<entity_id, struct_name> CONCAT(name, s);\
    public:\
        const struct_name& CONCAT(get_, CONCAT(name, _componet_instance)) (entity_id id) const;\
        struct_name& CONCAT(get_, CONCAT(name, _componet_instance)) (entity_id id);\
        Entity& CONCAT(add_, CONCAT(name, _component))(Entity& ent);\
        template<>\
        Entity& add_component<name>(Entity& ent){\
            return CONCAT(add_, CONCAT(name, _component))(ent);\
        }

#include "../Components/components.def"

#undef ADD_COMPONENT
    public:
        Entity_manager() = delete;

        explicit Entity_manager(observer_array* obs_ptr) : obs_ptr(obs_ptr){}

        Entity_manager(Entity_manager&& ) = default;

        Entity_manager& operator=(Entity_manager&&) = default;

        template <comp_type head, comp_type ... tail>
        Entity& add_components(Entity& ent){
            return add_component<head>(add_components<tail ...>(ent));
        };
        template<class = void>
        Entity& add_components(Entity& ent){
            return ent;
        }

        template <comp_type ... components>
        Entity& spawn_entity(){
            Entity ent(cur_id++);
            entities[ent.id] = ent;
            return add_components<components ...>(entities[ent.id]);
        }

        template <class = void>
        Entity& spawn_entity(){
            auto ent = Entity(cur_id++);
            entities[ent.id] = ent;
            return entities[ent.id];
        }

    };
}


#endif //ECS_ENTITY_MANAGER_H
