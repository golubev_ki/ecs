//
// Created by Kirill on 26.01.2020.
//

#include <array>
#include "../utils.h"
#include "../../external_dependencies/magic_enum/include/magic_enum.hpp"

#ifndef ECS_ENTITY_H
#define ECS_ENTITY_H

namespace ecs {
    struct Entity {
        entity_id id = -1;
        std::array<bool, magic_enum::enum_count<comp_type>()> sig = {
#define ADD_COMPONENT(name, struct_name, impl) false,

#include "../Components/components.def"

#undef ADD_COMPONENT
        };

        explicit Entity(entity_id id) : id(id){}
    };
}


#endif //ECS_ENTITY_H
